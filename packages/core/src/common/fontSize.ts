// tslint:disable:object-literal-sort-keys
export const FontSize = {
    ONE: 1 as 1,
    TWO: 2 as 2,
    THREE: 3 as 3,
    FOUR: 4 as 4,
    FIVE: 5 as 5,
};
export type FontSize = typeof FontSize[keyof typeof FontSize];
